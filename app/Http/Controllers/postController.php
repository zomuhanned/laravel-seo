<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;
use App\Models\Post;
use DaveChild\TextStatistics as TS;

class postController extends Controller
{
    //

    public function createPost(Request $request){

        $post = Post::create([
            "title" => $request->title
        ]);
        return redirect()->back();    
    }
    public function checkSlug(Request $request) {    
        $slug = SlugService::createSlug(Post::class, 'slug', $request->title);
        return response()->json(['slug' => $slug]);
    }

    public function measureReadability(Request $request) {
        $textStatistics = new TS\TextStatistics;
        return response()->json(['readability' => $textStatistics->fleschKincaidReadingEase($request->text)]);
    }

    public function getTextLength(Request $request) {
        $textStatistics = new TS\Text;
        return response()->json(['textLength' => $textStatistics::textLength($request->text)]);
    }

    public function getLettersCount(Request $request) {
        $textStatistics = new TS\Text;
        return response()->json(['lettersCount' => $textStatistics::letterCount($request->text)]);
    }

    public function getWordsCount(Request $request) {
        $textStatistics = new TS\Text;
        return response()->json(['wordsCount' => $textStatistics::wordCount($request->text)]);
    }

    public function getSentencesCount(Request $request) {
        $textStatistics = new TS\Text;
        return response()->json(['sentencesCount' => $textStatistics::sentenceCount($request->text)]);
    }

}
