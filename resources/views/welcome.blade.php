<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    {{-- jQuery Script --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <title>Laravel with SEO</title>
  </head>
  <body>
    <div class="container mt-5">
        <div class="row">
            <h1 class="text-center my-3">
                Laravel with SEO Principles
            </h1>
            <form action="/" method="POST">
                @csrf
            <div class="mb-3">
                <label for="title" class="form-label">Title</label>
                <input type="text" name="title" class="form-control" id="title">
            </div>
            {{-- Check Slug --}}
            <script>
                $('#title').change(function(e) {
                    $.get('{{ url('check_slug') }}', 
                    { 'title': $(this).val() }, 
                        function( data ) {
                            $('#slug').val(data.slug);
                        }
                    );
                });
            </script>
            <div class="mb-3">
                <label for="slug" class="form-label">Slug</label>
                <input type="text" name="slug" class="form-control" id="slug">
            </div>
            <div class="mb-3">
                <label for="text" class="form-label">Text</label>
                <input type="text" name="text" class="form-control" id="text">
                Readability: <p id="readability"></p>
                Text Length: <p id="textLength"></p>
                <!-- Letters Count: <p id="lettersCount"></p>
                Words Count: <p id="wordsCount"></p>
                Sentences Count: <p id="sentencesCount"></p> -->
            </div>
            {{-- Check SEO --}}
            <script>
                $('#text').change(function(e) {
                    $.get('{{ url('measure_readability') }}', 
                    { 'text': $(this).val() }, 
                        function( data ) {
                            $('#readability').text(data.readability);
                        }
                    );
                    $.get('{{ url('text_length') }}', 
                    { 'text': $(this).val() }, 
                        function( data ) {
                            $('#textLength').text(data.textLength);
                        }
                    );

                });
            </script>
            <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

  </body>
</html>