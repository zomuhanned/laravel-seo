<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\postController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::post('/', [postController::class,'createPost']);
Route::get('check_slug', [postController::class,'checkSlug']);
Route::get('measure_readability', [postController::class,'measureReadability']);
Route::get('text_length', [postController::class,'getTextLength']);
// Route::get('letters_count', [postController::class,'getLettersCount']);
// Route::get('words_count', [postController::class,'getWordsCount']);
// Route::get('sentences_count', [postController::class,'getSentencesCount']);

